1. Apakah perbedaan antara JSON dan XML?
2. Apakah perbedaan antara HTML dan XML?

Jawaban:
1.  
- JSON tidak menggunakan end tag sedangkan XML menggunakan end tag
- JSON lebih pendek dan simpel sedangkan XML lebih panjang
- JSON lebih mudah untuk dibaca dan ditulis sedangkan XML lebih rumit
- JSON dapat menggunakan arrays sedangkan XML tidak

2. 	
- HTML digunakan untuk menampilkan data sedang XML digunakan untuk mentrasfer data
- HTML merupakan markup languange sedangkan XML merupakan standard markup languange yang mendefinisikan markup languange lainnya
- HTML tidak case sensitive sedangkan XML case sensitive
- HTML tidak selalu membutuhkan end tag sedangkan XML wajib