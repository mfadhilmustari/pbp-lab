import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Belajar Form Flutter",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BelajarFlutter.com"),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: [
              // tambahkan komponen seperti input field disini
              TextFormField(
                autofocus: true,
                decoration: new InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                    hintText: "Masukan Nama Lengkap Anda",
                    labelText: "Nama Lengkap",
                    icon: Icon(Icons.people)),
              ),
              TextFormField(
                autofocus: true,
                decoration: new InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                    hintText: "Masukan Universitas Anda",
                    labelText: "Nama Universitas",
                    icon: Icon(Icons.school_rounded)),
              ),
              TextFormField(
                  autofocus: true,
                  decoration: new InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                      hintText: "Masukan Jurusan Anda",
                      labelText: "Nama Jurusan",
                      icon: Icon(Icons.school_rounded))),
              ElevatedButton(onPressed: () {}, child: Text("Submitt"))
            ],
          ),
        ),
      ),
    );
  }
}
