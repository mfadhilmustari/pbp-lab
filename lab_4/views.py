from django.http import request
from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url="/admin/login/")
def index(request):
    notes = Note.objects.all().values()
    response = {'notes' : notes}
    return render(request, 'lab4_index.html', response)

@login_required(login_url="/admin/login/")
def add_note(request):
	context = {}

	form = NoteForm(request.POST or None, request.FILES or None)

	if form.is_valid():
		form.save()

	context['form'] = form
	return render(request, "lab4_form.html", context)

# @login_required(login_url="/admin/login/")
# def note_list(request):
# 	notes = Note.objects.all().values()
#     response = { 'notes' : notes}
#     return render(request, 'lab4_index.html', response)