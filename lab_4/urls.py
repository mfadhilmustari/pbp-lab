from django.urls import path, include
from lab_4.views import index, add_note

urlpatterns = [
    path('', index, name= 'index'),
    path('add-note/', add_note, name='add'),
    # path('note-list/', note_list, name='note'),
]