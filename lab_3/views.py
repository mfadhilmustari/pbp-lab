from django.shortcuts import render
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all().values()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login/")
def add_friend(request):
	context = {}

	form = FriendForm(request.POST or None, request.FILES or None)

	if form.is_valid():
		form.save()

	context['form'] = form
	return render(request, "lab3_form.html", context)